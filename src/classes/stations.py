#!/usr/bin/env python
# coding: utf-8

# In[ ]:


class stations:
    def get_stations(self,path= "../gps_stations/with_LoadDef/Observed_OTL-induced_Displacements_M2.txt"):
        with open(path) as file:
            header   = file.readline()
            stations = pd.read_csv(file, delim_whitespace=True, index_col=0, header=None, names=header.strip().split(' | ') )

        receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                             side_set_name='r1', station_code=name, fields=["displacement"]) 
                                             for name, lat, lon in zip(stations.index,stations['Lat(+N,deg)'], stations['Lon(+E,deg)']) ]
        return receivers
    
    def read_loaddef_data(self,path= "../gps_stations/with_LoadDef/Observed_OTL-induced_Displacements_M2.txt"):
        with open(path) as file:
            header   = file.readline()
            stations = pd.read_csv(file, delim_whitespace=True, index_col=0, header=None, names=header.strip().split(' | ') )

        receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                             side_set_name='r1', station_code=name, fields=["displacement"]) 
                                             for name, lat, lon in zip(stations.index,stations['Lat(+N,deg)'], stations['Lon(+E,deg)']) ]
        station_measurements = { name:         np.array([[lat, lon],[abs(complex(eamp + math.radians(eph))), 
                                                             abs(complex(namp + math.radians(nph))), 
                                                             abs(complex(vamp + math.radians(vph)))]], dtype=list) for name, lat, lon, eamp, eph, namp, nph, vamp, vph in 
                                                                                                                        zip(stations.index
                                                                                                                       , stations['Lat(+N,deg)'], stations['Lon(+E,deg)']
                                                                                                                       , stations['E-Amp(mm)'],   stations['E-Pha(deg)']
                                                                                                                       , stations['N-Amp(mm)'],   stations['N-Pha(deg)']
                                                                                                                       , stations['V-Amp(mm)'],   stations['V-Pha(deg)']) }
        
        return station_measurements
            
    def get_loaddef_data_full(self,path= "../gps_stations/with_LoadDef/Observed_OTL-induced_Displacements_M2.txt"):
        with open(path) as file:
            header   = file.readline()
            stations = pd.read_csv(file, delim_whitespace=True, index_col=0, header=None, names=header.strip().split(' | ') )
        return stations
    
    def get_rotational_matrix(self, theta, phi):
        theta = np.radians(90-theta) #latitude 
        rotx = np.array([[1,             0,              0 ],
                         [0, np.cos(theta), -np.sin(theta) ],
                         [0, np.sin(theta),  np.cos(theta) ] ])

        phi = np.radians(phi) #longitude
        rotz = np.array([[np.cos(phi), -np.sin(phi),   0],
                         [np.sin(phi),  np.cos(phi),   0],
                         [          0,          0,     1]])
        return rotx.dot(rotz)
    
    def rotate_vector(self, vector, crd):
        rotm = self.get_rotational_matrix(crd[0], crd[1]) #lat,lon
        return rotm.dot(vector)
    
        
    def read_receivers_results(self, path = '../solutions/elastostatic/receivers.h5', field="displacement"):
        with h5py.File(path) as file:
            data    = np.array(file['point'][field])[:,:,1]
            stnames = np.array(file['names_ELASTIC_point'], dtype=str)
        result = {}
        for station, record in zip(stnames, data):
            result[station[3:-1]] = record * 1000. # in mm
        return result

