from octiload_class import *

class elastostatic_solver(octiload):
    
    def __init__(self, meshfile = '../meshes/mesh.h5'):
        self.config   = config()
        self.meshfile = meshfile
        if not os.path.exists(os.path.dirname(meshfile)): os.makedirs(os.path.dirname(meshfile))
                                                                      
    def prepare_mesh_fields(self, rho_water=1030., real_or_imag ='re'):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        assert (real_or_imag in ['re', 'im'])
        mesh = self.mesh
        self.rho_water = rho_water

        mesh.attach_field('fluid', np.zeros(mesh.nelem))
        mesh.attach_field('zeros', 0. * mesh.elemental_fields['tidal_elevation_amp'])
        mesh.map_nodal_fields_to_element_nodal()
        if real_or_imag == "re":
            NEUMANN = self.rho_water * self.mesh.elemental_fields['tidal_elevation_'+real_or_imag]
        elif real_or_imag == "im":
            NEUMANN = self.rho_water * self.mesh.elemental_fields['tidal_elevation_'+real_or_imag]
        mesh.attach_field('NEUMANN', NEUMANN)
        #removing all unnessesary fields
        del self.mesh.element_nodal_fields['QMU']
        del self.mesh.element_nodal_fields['QKAPPA']

    def read_gps_data(self, path= "../gps_stations/with_LoadDef/Observed_OTL-induced_Displacements_M2.txt"):
        with open(path) as file:
            header   = file.readline()
            stations = pd.read_csv(file, delim_whitespace=True, index_col=0, header=None, names=header.strip().split(' | ') )

        receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                             side_set_name='r1', station_code=name, fields=["displacement"]) 
                                             for name, lat, lon in zip(stations.index,stations['Lat(+N,deg)'], stations['Lon(+E,deg)']) ]
        self.stations = receivers
        
    
    def run_simulation(self, stations=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        mesh                                             = self.mesh
        assert ("r0" in mesh.side_sets.keys())
        assert ("r1" in mesh.side_sets.keys())
        sim                                              = sn.simple_config.simulation.Elastostatic(mesh=mesh)
        sim.domain.polynomial_order                      = mesh.shape_order
        sim.physics.elastostatic_equation.mass_matrix_scaling = False
        
        sim.physics.elastostatic_equation.gravity = "full"
        sim.physics.elastostatic_equation.right_hand_side.filename = self.meshfile
        sim.physics.elastostatic_equation.right_hand_side.format   = "hdf5"
        sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros", "zeros", "zeros"]

        sim.physics.elastostatic_equation.solution.filename        = os.path.basename(self.config.elastos_run.volume_solution)
        sim.physics.elastostatic_equation.solution.fields          = self.config.elastos_run.volume_solution_fields
        
        boundaries = simple_config.boundary.Neumann(
            side_sets=["r1"]
        )
        sim.add_boundary_conditions(boundaries)
        
        boundaries = simple_config.boundary.HomogeneousDirichlet(
            side_sets=['r0']
        )
        sim.add_boundary_conditions(boundaries)

        sim.solver.max_iterations     = self.config.elastos_run.max_iterations
        sim.solver.absolute_tolerance = self.config.elastos_run.absolute_tolerance
        sim.solver.relative_tolerance = self.config.elastos_run.relative_tolerance
        sim.solver.preconditioner     = self.config.elastos_run.preconditioner
        
        if type(stations) != bool:
            if hasattr(self, "stations"):
                if str(stations) == 'all':
                    sim.add_receivers(self.stations)
                    self.used_stations = self.stations.copy()
                else:
                    sim.add_receivers(stations)
                    self.used_stations = stations
                sim.output.point_data.format                          = "hdf5"
                sim.output.point_data.filename                        = os.path.basename(self.config.elastos_run.point_solution)
                sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations
            
        sim.validate()

        sn.api.run(
            input_file    = sim,
            site_name     = self.config.run.site_name,
            output_folder = self.config.elastos_run.solution_folder,
            overwrite     = True,
            ranks         = self.config.run.ranks,
            wall_time_in_seconds = self.config.run.wall_time_in_seconds
        )
        
    def read_simulation_results(self, field_names = ['solution_X', 'solution_Y', 'solution_Z']):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        assert len(field_names) == 3, "Number of field names must be 3"
        path = self.config.elastos_run.volume_solution
        solution = sn.UnstructuredMesh.from_h5(path)
        for i,field in enumerate(['solution_X', 'solution_Y', 'solution_Z']):
            self.mesh.attach_field(field_names[i], solution.element_nodal_fields[field])
    
    def read_receivers_results(self):
        assert hasattr(self, "stations"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        with h5py.File(self.config.elastos_run.point_solution) as file:
            data = np.array(file['point']['displacement'])
        receivers_record = data[:, :, 1]
        result = {}
        for i, rec in enumerate(self.used_stations):
            result[rec.name[3:-1]] = receivers_record[i]*1000.
        self.used_stations_data = result
        return result
    
    def read_gps_data(self, path= "../gps_stations/with_LoadDef/Observed_OTL-induced_Displacements_M2.txt"):
        with open(path) as file:
            header   = file.readline()
            stations = pd.read_csv(file, delim_whitespace=True, index_col=0, header=None, names=header.strip().split(' | ') )

        receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                             side_set_name='r1', station_code=name, fields=["displacement"]) 
                                             for name, lat, lon in zip(stations.index,stations['Lat(+N,deg)'], stations['Lon(+E,deg)']) ]
        self.stations = receivers
        self.full_gps_dataset = stations
        self.stations_measurements = { name: [abs(complex(eamp + math.radians(90-eph))), 
                                              abs(complex(namp + math.radians(90-nph))), 
                                              abs(complex(vamp + math.radians(90-vph)))] for name, eamp, eph, namp, nph, vamp, vph in zip(stations.index
                                                                                                   , stations['E-Amp(mm)'], stations['E-Pha(deg)']
                                                                                                   , stations['N-Amp(mm)'], stations['N-Pha(deg)']
                                                                                                   , stations['V-Amp(mm)'], stations['V-Pha(deg)']) }