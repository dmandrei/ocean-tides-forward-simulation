from salvus.mesh import UnstructuredMesh
import numpy as np
from pympler import asizeof
import scipy.interpolate as si
from netCDF4 import Dataset
from mass.elemental_matrices import get_mass_matrix
from salvus.mesh.models_1D import model 

def get_sideset_mask(mesh, sideset):
        assert type(mesh) == UnstructuredMesh
        assert sideset in mesh.side_sets.keys()
        facets = { 'to=2':
                     {'0':  list(range(  0,   9)),
                      '1':  list(range( 18,  27)),
                      '2':  list(range(  0,   3)) + \
                            list(range(  9,  12)) + \
                            list(range( 18,  21)),
                      '3':  list(range(  6,   9)) + \
                            list(range( 15,  18)) + \
                            list(range( 24,  27)),
                      '4':  list(range(2, 27, 3)),
                      '5':  list(range(0, 27, 3)),},

                   'to=4':
                     {'0':  list(range(0,25)),
                      '1':  list(range(100,125)),
                      '2':  list(range(  0,   5)) + \
                            list(range( 25,  30)) + \
                            list(range( 50,  55)) + \
                            list(range( 75,  80)) + \
                            list(range(100, 105)),
                      '3':  list(range( 20,  25)) + \
                            list(range( 45,  50)) + \
                            list(range( 70,  75)) + \
                            list(range( 95, 100)) + \
                            list(range(120, 125)),
                      '4':  list(range(4,125,5)),
                      '5':  list(range(0,125,5)),}
             }

        tensor_order = int(np.rint((mesh.nodes_per_element)**(1/3.)) - 1) #it's redundant but clearer
        assert tensor_order in set([2,4])
        n_nodes_per_element = (tensor_order+1)**3
        n_nodes_facet       = (tensor_order+1)**2
        print(mesh.nodes_per_element, tensor_order)
        elements = mesh.side_sets[sideset][0]
        edges    = mesh.side_sets[sideset][1]
        mask = (np.repeat(elements, n_nodes_facet), np.array([facets[f'to={tensor_order}'][str(edge)] for edge in edges]).ravel())
        return mask

def cart2geogr(coordinates):
    radii = np.linalg.norm(coordinates, axis=-1)
    longitude = np.degrees(np.arctan2(coordinates[:,1], coordinates[:,0])) + 180
    latitude  = np.degrees(np.arcsin(coordinates[:,2] / radii))
    assert np.min(longitude) >= 0.
    assert np.max(longitude) <= 360.
    assert np.min(latitude) >= -90.
    assert np.max(latitude) <= 90.
    return latitude, longitude

def get_mesh_dims(mesh):
    assert type(mesh) == UnstructuredMesh
    return (mesh.nelem, mesh.nodes_per_element)

def objectsize(obj):
    #obj size in bytes:
    size = asizeof.asizeof(obj) 
    count = 0
    units = ['b', 'Kb', 'Mb', 'Gb']
    while (size >= 1024.):
        size /= 1024.
        count += 1
    return f'{size:.2f} {units[count]}'
    
def get_parameter_integral(mesh, param, mass_matrix, mask=''):
    assert get_mesh_dims(mesh) == mass_matrix.shape
    if type(mask) == np.ndarray:
        assert mask.shape[0] ==  mass_matrix.shape[0]
        return np.abs(np.sum(mass_matrix[mask] * mesh.element_nodal_fields[param][mask]))
    else:
        return np.abs(np.sum(mass_matrix * mesh.element_nodal_fields[param]))

def reassign_parameter(mesh, parameter="RHO", oneD_model='prem_iso_one_crust', R_Earth=6371e3):
    assert parameter in set(["RHO", "VP", "VS"])
    radii = np.linalg.norm(mesh.points[mesh.connectivity], axis=-1)
    param_full = np.zeros_like(radii)
    elem_centorids = mesh.get_element_centroid_radius() 
    m     = model.built_in("prem_iso_one_crust")

    mask  = elem_centorids < R_Earth
    elem_centorids = elem_centorids[mask].repeat(mesh.nodes_per_element) / R_Earth
    rel_radii = radii[mask] / R_Earth
    shape = rel_radii.shape

    rel_radii = rel_radii.ravel()

    param_inside_earth = m.get_elastic_parameter(parameter_name=parameter, radius=rel_radii, element_centroids = elem_centorids)
    param_full[mask] = param_inside_earth.reshape(shape)

    mesh.attach_field(parameter, param_full)
    
def get_oneD_gravpot(mesh, oneD_model='prem_iso_one_crust', R_Earth=6371e3):
    m = model.built_in(oneD_model)
    potential_field = m.get_gravitational_potential(np.linalg.norm(mesh.points, axis=1)/R_Earth)[mesh.connectivity]
    mesh.attach_field('oneD_gravpot', potential_field)
    
##############################################################################
### tools for getting surface fields from netCDF4 files and interpolate it ###
##############################################################################
def attach_surf_field_2_mesh(mesh, surface_field, mask, fieldname = 'new_field'):
    ## attaches array for a surface mesh to the corresponding volumetric mesh. Mask is required to be provided.
    field2att = np.zeros((mesh.nelem, mesh.nodes_per_element))
    field2att[mask] = surface_field
    mesh.attach_field(fieldname, field2att)
    
def read_nc_and_get_interp_function(filename, lmax = 64, datafield="re", interpolationk="cubic"):
    '''
    read nc (netCDF4) file, extract longitudes, latitudes and data fields, and outputs interpolation fucntion
    all dimensions and variables have to have names staring of the format {longitude/latitude/}_tidal_elevation_{re/im}_lmax_{16/32/64/128/256/512/1024/2048/4096/10800}
    '''
    data = Dataset(filename, 'r', format="NETCDF4")
    assert lmax in (16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 10800)
    assert datafield in ("re", "im")

    long      = np.array(data[f'longitude_tidal_elevation_{datafield}_lmax_{lmax}'])
    lat       = np.array( data[f'latitude_tidal_elevation_{datafield}_lmax_{lmax}'])
    parameter = np.array(          data[f'tidal_elevation_{datafield}_lmax_{lmax}'])
    data.close()
    
    long_m, lat_m = np.meshgrid(long, lat)

    f = si.interp2d(lat, long, parameter, kind=interpolationk)
    return f


def apply_interpolation_function(f, surf_lat, surf_long):
    '''
    get surface field from interpolation (interp2d) function
    '''
    assert type(f) == si.interpolate.interp2d
    return si.dfitpack.bispeu(f.tck[0], f.tck[1], f.tck[2], f.tck[3], f.tck[4], surf_lat, surf_long)[0]
    
    
    