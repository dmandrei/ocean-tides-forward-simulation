import numpy as np
from pathlib import Path
from pympler import asizeof
from salvus.mesh import UnstructuredMesh

from classes.tools import *



def sideset_maskout(mesh, sideset_name='r1', 
                    save=False, output_filename='masked_mesh.h5'):
    assert sideset_name in mesh.side_sets.keys()
    print(f"Input mesh size: {objectsize(mesh)}")
    surf_elem_idx = mesh.side_sets[sideset_name][0]
    mesh_dims = get_mesh_dims(mesh)
    element_mask = np.zeros(mesh_dims[0], dtype=bool)
    element_mask[surf_elem_idx] = True
    if 'external' in mesh.elemental_fields.keys():
        element_mask[mesh.elemental_fields['external'] == 1] = False
    masked_mesh = mesh.apply_element_mask(element_mask)
    print(f"Masked mesh size: {objectsize(masked_mesh)}")
    if save: 
        if not Path(output_filename).exists(): p.parent.mkdir()
        masked_mesh.write_h5(output_filename)
    return masked_mesh

def interior_maskout(mesh,
                   save=False, output_filename='masked_mesh.h5'):
    print(f"Input mesh size: {objectsize(mesh)}")
    if 'external' in mesh.elemental_fields.keys():
        mask = np.array((mesh.elemental_fields['external']==0), dtype=bool)
        masked_mesh = mesh.apply_element_mask(mask)
    else:
        R_earth = 6371e3
        mask = np.array(mesh.get_element_centroid_radius() <= R_earth, dtype=bool)
        masked_mesh = mesh.apply_element_mask(mask)
    print(f"Masked mesh size: {objectsize(masked_mesh)}")
    if save: 
        p = Path(output_filename)
        if not p.parent.exists(): p.parent.mkdir()
        masked_mesh.write_h5(output_filename)
    return masked_mesh

def clipmesh(mesh, axis=0,
        save=False, output_filename='masked_mesh.h5'):
    print(f"Input mesh size: {objectsize(mesh)}")
    #axes: 0:x, 1:y, 2:z
    assert axis in [0,1,2]
    mask = np.array(mesh.get_element_centroid()[:, axis] > 0, dtype=bool)
    masked_mesh = mesh.apply_element_mask(mask)
    print(f"Masked mesh size: {objectsize(masked_mesh)}")
    if save:
        p = Path(output_filename)
        if not p.parent.exists(): p.parent.mkdir()
        masked_mesh.write_h5(output_filename)
    return masked_mesh

def get_slice(mesh, axis=0, slice_thickness_factor = 1.5,
             save=False, output_filename='masked_mesh.h5'):
    assert axis in [0,1,2]
    
    el_centres = mesh.get_element_centroid()[:,axis]
    min_dist   = np.min( np.abs(el_centres))
    
    mask = np.array(((el_centres > 0) and (el_centres < (min_dist * 8))), dtype=bool) - np.array(el_centres < (min_dist * 10), dtype=bool) 
    masked_mesh = mesh.apply_element_mask(mask)
    if save:
        p = Path(output_filename)
        if not p.parent.exists(): p.parent.mkdir()
        masked_mesh.write_h5(output_filename)
    return masked_mesh


# ds_extract_3d_sph = xr.Dataset(
#     coords={
#         "longitude": np.linspace(0, 0, 1),
#         "latitude": np.linspace(0, 0, 1),
#         "radius": np.linspace(0, max_radius, num_samples),
#     },
# )
# ds_extract_3d_sph = extract_model_to_regular_grid(
#     solution, ds_extract_3d_sph, ["solution"], verbose=True, 
#     max_tree_doublings='auto'
# )