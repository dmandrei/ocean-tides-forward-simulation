#!/bin/bash

if [ -z "$(pgrep -u andreid ssh-agent)" ]
then
  echo "launching ssh-agent..."
  eval "$(ssh-agent)"
  ssh-add ~/.ssh/id_25519_swppc
else
  echo "ssh-agent is launched"
fi
if [ "$(ssh-add -l)" !=  "The agent has no identities." ]
then
  echo "there are some keys"
else
  echo "there are no keys"
  ssh-add ~/.ssh/id_ed25519_swppc
fi   
