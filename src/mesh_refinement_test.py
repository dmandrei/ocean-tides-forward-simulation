#!/usr/bin/env python
# coding: utf-8



import time
import os
import numpy as np

from salvus.mesh.models_1D import model 
from salvus.mesh import UnstructuredMesh
from salvus.mesh.tools.transforms import uniformly_refine_chunkwise
from classes.poisson import poisson_solver
import classes.tools as ts
import subprocess
import time


start = time.time()
ro = 2 #refinement order
n_refinements = 2
solution_integral = {}

a = poisson_solver(meshfile = '/home/andreid/projects/ocean-tides-forward-simulation/meshes/gravity/mesh.h5')
a.config.run.tensor_order = 1 # has to be 1 to further refine. Will be changed later
a.construct_mesh(nex = 24, buffer=10, oneD_model="prem_iso_one_crust")
for field in list(a.fields - set(['layer', 'external'])):
    del a.mesh.elemental_fields[field]

for to in [1]:
    print(f"\n\nTensor order {to}\nTime: {time.time()-start}\n\n")
    for dummy in range(n_refinements+1):
        if dummy > 0:
            print(f"START: uniformly_refine_chunkwise. TIME: {time.time()-start}")
            test =  uniformly_refine_chunkwise(
                mesh=a.mesh,
                output_filename=a.meshfile,
                nchunk=8 ** (dummy+1),
                uniformly_refine_kwargs={
                    "tensor_order":to,
                    "refinement_order":ro
                }
            )
            print(f"END: uniformly_refine_chunkwise.   TIME: {time.time()-start}" )

            print(f"START: Loading mesh.") 
            a.mesh = UnstructuredMesh.from_h5(a.meshfile) 
            print(f"END: Loading mesh. TIME: {time.time()-start}")
            print(f"START: reassign density")
            for param in ['RHO']:
                ts.reassign_parameter(a.mesh, parameter='RHO', oneD_model=a.oneD_model)
            print(f"END: reassign density. TIME: {time.time()-start}")
            print(f"START: get mass matrix.")
            mass_matrix = ts.get_mass_matrix(a.mesh)
            print(f"END: get mass matrix. TIME: {time.time()-start}")
            mass = np.sum(mass_matrix * a.mesh.elemental_fields['RHO'])

            a.prepare_mesh_fields()
            a.mesh.elemental_fields['M1'] = np.ones(a.mesh.nelem)
            a.add_oneD_solution(npoints = 100)
            a.write_mesh(a.meshfile)

            if (a.mesh.nelem > 1e6) or (to > 2): 
                host = "daint.cscs.ch"
                remote_path = "/scratch/snx3000/admitrov/mesh.h5"
                a.config.run.ranks=1200
                a.config.run.site_name='daint'
                #uploading mesh to piz daint
                print("START: uploading mesh to Piz Daint scratch.TIME: {time.time()-start}")
                subprocess.run(['./bash_scripts/scp_mesh.sh', a.meshfile, host, remote_path])
                print("END: uploading mesh to Piz Daint scratch. TIME: {time.time()-start}")
                a.run_simulation(remote_path=remote_path)
            else:
                a.config.run.ranks=10
                a.config.run.site_name='local'
                a.run_simulation()

            print("START: read simulation results. TIME: {time.time()-start}")
            a.read_simulation_results()
            print("END: read simulation results. TIME: {time.time()-start}")
            mass_matrix = ts.get_mass_matrix(a.mesh)
            a.mesh.attach_field('mass_matrix', mass_matrix)
            if 'oneD_gravpot' not in a.mesh.elemental_fields.keys():
                ts.get_oneD_gravpot(a.mesh, oneD_model=a.oneD_model)
            a.write_mesh(a.meshfile)
            
            mask = (a.mesh.get_element_centroid_radius() < 6371e3)
            integral_analyt = np.sum(a.mesh.elemental_fields['mass_matrix'][mask] * a.mesh.elemental_fields['oneD_gravpot']  [mask] )
            integral_numerc = np.sum(a.mesh.elemental_fields['mass_matrix'][mask] * a.mesh.elemental_fields['grav_potential'][mask] )
            integral_differ = np.sum(a.mesh.elemental_fields['mass_matrix'][mask] * (np.abs(a.mesh.elemental_fields['grav_potential'][mask]) - np.abs(a.mesh.elemental_fields['oneD_gravpot'][mask])) )
            
            solution_integral[a.mesh.nelem] = {'n elements': a.mesh.nelem,
                                               'Earth mass': mass,
                                               'Earth volume': np.sum(mass_matrix[mask]),
                                               'tensor order': to,
                                               'num_sol_integral'   : integral_numerc,
                                               'anl_sol_integral'   : integral_analyt,
                                               'numer/analyt'       : (1 - np.abs(integral_numerc /integral_analyt)),
                                               'integral difference': integral_differ,
                                               'difference/analyt'  : integral_differ /integral_analyt}
            print(solution_integral) 
            with open('solutions_integral.txt', 'a') as f:
                f.write(str(solution_integral))
