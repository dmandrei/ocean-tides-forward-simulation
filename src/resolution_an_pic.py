#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
import numpy as np


# In[82]:


results = [        {'n elements': 48512, 
                     'Earth mass': 5.9460977647202918e+24, 
                     'Earth volume': 1.0825412601575515e+21, 
                     'tensor order': 1, 
                     'num_sol_integral': 8.1834496931066217e+28, 
                     'anl_sol_integral': -8.4611250561444306e+28, 
                     'numer/analyt': 0.032817782646547999, 
                     'integral difference': -2.7767536303781029e+27, 
                     'difference/analyt': 0.032817782646548131},
                   {'n elements': 388096, 
                    'Earth mass': 5.9516983898446206e+24, 
                    'Earth volume': 1.0815171583538836e+21, 
                    'tensor order': 1, 
                    'num_sol_integral': 8.3758514042636965e+28, 
                    'anl_sol_integral': -8.4710045279552156e+28, 
                    'numer/analyt': 0.011232802836724232, 
                    'integral difference': -9.5153123691522463e+26, 
                    'difference/analyt': 0.011232802836724622},
                   {'n elements': 24838144, 
                    'Earth mass': 5.9674120482416698e+24, 
                    'Earth volume': 1.0811971265402352e+21, 
                    'tensor order': 1, 
                    'num_sol_integral': 8.4607151551103926e+28, 
                    'anl_sol_integral': -8.4738506081362943e+28, 
                    'numer/analyt': 0.0015501161907774952, 
                    'integral difference': -1.3135453025911402e+26, 
                    'difference/analyt': 0.001550116190778629},
                   {'n elements': 48512, 
                     'Earth mass': 5.953364523110126e+24, 
                     'Earth volume': 1.0811757910859946e+21, 
                     'tensor order': 2, 
                     'num_sol_integral': 8.4411921047122708e+28, 
                     'anl_sol_integral': -8.4740351650899513e+28, 
                     'numer/analyt': 0.0038757285918499251, 
                     'integral difference': -3.2843060377678594e+26, 
                     'difference/analyt': 0.0038757285918496619},

                   {'n elements': 388096, 
                    'Earth mass': 5.9533809340057465e+24, 
                    'Earth volume': 1.0811757910859944e+21, 
                    'tensor order': 2, 
                    'num_sol_integral': 8.4444653732875304e+28, 
                    'anl_sol_integral': -8.4740360426540335e+28, 
                    'numer/analyt': 0.0034895614342043135, 
                    'integral difference': -2.9570669366501624e+26, 
                    'difference/analyt': 0.0034895614342041682},
                   {'n elements': 3104768, 
                    'Earth mass': 5.9695371196991872e+24, 
                    'Earth volume': 1.0811757910859931e+21, 
                    'tensor order': 2, 
                    'num_sol_integral': 8.4681311947096284e+28, 
                    'anl_sol_integral': -8.4740362736193196e+28, 
                    'numer/analyt': 0.00069684371402500833, 
                    'integral difference': -5.9050789096961994e+25, 
                    'difference/analyt': 0.00069684371402555954},
                   {'n elements': 48512, 
                    'Earth mass': 5.95338202979855e+24, 
                    'Earth volume': 1.0811757910859953e+21, 
                    'tensor order': 4, 
                    'num_sol_integral': 8.4447195305923871e+28, 
                    'anl_sol_integral': -8.4740366398643998e+28, 
                    'numer/analyt': 0.0034596391917987024, 
                    'integral difference': -2.9317109272012261e+26, 
                    'difference/analyt': 0.0034596391917986078},
                   {'n elements': 388096,
                     'Earth mass': 5.9533820297951462e+24, 
                     'Earth volume': 1.0811757910859949e+21, 
                     'tensor order': 4, 
                     'num_sol_integral': 8.4447244262621423e+28, 
                     'anl_sol_integral': -8.4740362793811652e+28, 
                     'numer/analyt': 0.0034590190733952975, 
                     'integral difference': -2.9311853118996416e+26, 
                     'difference/analyt': 0.0034590190733921403},
                

          ]


# In[83]:


elements   = np.array([run['n elements'] for run in results])
difference = np.array([run['difference/analyt'] for run in results])
tos        = np.array([run['tensor order'] for run in results])


# In[84]:




# In[85]:


to = 1
mask = (tos == to)
plt.loglog(elements[mask], difference[mask], 'o--', color='r', label = f'to={to}')

to = 2
mask = (tos == to)
plt.loglog(elements[mask], difference[mask], 'o--', color='b', label = f'to={to}')


to = 4
mask = (tos == to)
plt.loglog(elements[mask], difference[mask], 'o--', color='g', label = f'to={to}')

plt.legend()

plt.savefig('resolution_an_pic.png')
# In[ ]:



