import os.path as op
import numpy   as np

from salvus.mesh.models_1D import model
from salvus.mesh import UnstructuredMesh

import classes.tools as ts

R_Earth = 6371e3

print("enter path to mesh:")
meshpath = input()
while not op.isfile(meshpath):
    print("there is no file under the provided path. try again:")
    meshpath = input()
    if (meshpath == "exit") or (meshpath == "quit") or (meshpath == "q"): exit()
     
mesh = UnstructuredMesh.from_h5(meshpath)

if 'mass_matrix' not in mesh.elemental_fields.keys():
    mass_matrix = ts.get_mass_matrix(a.mesh)
    mesh.attach_field('mass_matrix', mass_matrix)

if 'oneD_gravpot' not in mesh.elemental_fields.keys():
    print('there\'s no analytical solution in the mesh. I\'m going to add it. What 1D model should I use? ')
    oneD_model = input()
    if oneD_model == '': oneD_model = 'prem_iso_one_crust'
    ts.get_oneD_gravpot(mesh, oneD_model=oneD_model)
    
mask = ( mesh.get_element_centroid_radius() < R_Earth )

integral_analyt = np.sum(mesh.elemental_fields['mass_matrix'][mask] * mesh.elemental_fields['oneD_gravpot']  [mask] )
integral_numerc = np.sum(mesh.elemental_fields['mass_matrix'][mask] * mesh.elemental_fields['grav_potential'][mask] )
integral_differ = np.sum(mesh.elemental_fields['mass_matrix'][mask] * (np.abs(mesh.elemental_fields['grav_potential'][mask]) - np.abs(mesh.elemental_fields['oneD_gravpot'][mask])) )
print(f'Integral of analytical solution: {integral_analyt}')
print(f'Integral of numerical solution:  {integral_numerc}')
print(f'numer/analyt: {integral_numerc / integral_analyt}')
print(f'integral difference: {integral_differ}')
print(f'difference/analyt: {integral_differ / integral_analyt}')




print('rewrite mesh? (oneD solution and mass matrix attached)')
print('only "yes" counts')
answer = input()
if answer == "yes":
    mesh.write_h5(meshpath)