#!/usr/bin/env python
# coding: utf-8

# In[12]:


import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import time
import random
import sys

from classes.poisson import poisson_solver

import matplotlib
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 12}

matplotlib.rc('font', **font)


# In[2]:


buf_comp = {}


# In[42]:


for buf in [1,5,10,15,21,30]:
    a = poisson_solver(meshfile = '../meshes/gravity/mesh.h5')
    a.config.run.tensor_order = 4
    a.config.gravity_run.boundary_condition = "dirichlet"
    with_surf_topo = True
    with_moho_topo = True
    a.construct_mesh(with_surface_topo=with_surf_topo, 
                     with_moho_topo=with_moho_topo,
                     nex = 24, buffer=buf, )
    a.add_oneD_solution(npoints = 100)
    a.prepare_mesh_fields()
    a.write_mesh(path = a.meshfile)
    a.run_simulation()
    buf_comp[str(buf)] = a.compare_solution_with_analytical()


# In[48]:


xvect, yvect = list(), list()
keys = np.array(list(buf_comp.keys()),dtype=int)
keys.sort()
for key in keys:
    xvect.append(key)
    yvect.append(buf_comp[str(key)])
plt.plot(xvect, np.abs(yvect), '-')
plt.xlabel("buffer thickness, elements")
plt.ylabel("$\dfrac{||\int \phi d\Omega - \int \phi_{analyt} d\Omega||_2}{||\int \phi_{analyt} d\Omega||_2}$, %")
plt.xlim([0,np.max(keys)])
if (with_surf_topo) and (not with_moho_topo):
    plt.savefig("buffer_thickness_topo_nomoho.pdf")
elif (with_surf_topo) and (with_moho_topo):
    plt.savefig("buffer_thickness_topo_moho.pdf")


# In[ ]:




