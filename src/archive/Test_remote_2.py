#!/usr/bin/env python
# coding: utf-8

# In[1]:

import salvus.namespace as sn
from salvus.mesh import simple_mesh
import numpy as np
G = 6.67*10**-11
M1 = (4 * G * np.pi)**-1

def create_mesh(where2save):
    m = simple_mesh.TidalLoading()
    m.basic.model = 'prem_iso_one_crust'
    m.gravity_mesh.add_exterior_domain = True
    m.gravity_mesh.nelem_buffer_outer  = 10
    m.advanced.tensor_order = 2
    mesh = m.create_mesh()

    mesh.attach_field('rhs', mesh.elemental_fields['RHO'])
    f =  np.ones_like(mesh.elemental_fields['RHO'])
    mesh.attach_field('fluid', np.ones(mesh.nelem))
    mesh.attach_field('M0', f)
    mesh.attach_field('M1', f * M1)
    
    for field in ['RHO', 'VP', 'VS', 'QMU', 'QKAPPA', 'GRAD_PHI_X', 'GRAD_PHI_Y', 'GRAD_PHI_Z', 'g']:
        del mesh.elemental_fields[field]
    
    mesh.write_h5(where2save)
    return mesh

site_name = 'daint'

if site_name == 'daint':
    remote = True
    ranks  = 120
elif site_name == 'local':
    remote = False
    ranks  = 10


if remote:
    remote_path = "REMOTE:/scratch/snx3000/admitrov/mesh.h5"

    
    
    sim = sn.simple_config.simulation.Poisson()

    sim.domain.dimension = 3
    sim.domain.polynomial_order = 2
    sim.domain.geometry.format = "hdf5"
    sim.domain.geometry.filename = remote_path
    sim.domain.mesh.format = "hdf5"
    sim.domain.mesh.filename = remote_path
    sim.domain.model.format = "hdf5"
    sim.domain.model.filename = remote_path

    sim.physics.poisson_equation.right_hand_side.filename = remote_path

else:
    local_path = "../meshes/gravity/mesh.h5"
    mesh = create_mesh(local_path)
    
    sim                                              = sn.simple_config.simulation.Poisson(mesh=mesh)
    sim.domain.polynomial_order                      = mesh.shape_order

    sim.physics.poisson_equation.mass_matrix_scaling = False

    sim.physics.poisson_equation.right_hand_side.filename = local_path
            
sim.physics.poisson_equation.right_hand_side.format = "hdf5"
sim.physics.poisson_equation.right_hand_side.field = "RHS"

sim.physics.poisson_equation.solution.filename = "solution.h5"
sim.physics.poisson_equation.solution.fields = ["solution", "residuals"]

boundaries = sn.simple_config.boundary.HomogeneousDirichlet(
    side_sets=["r2"]
)

# Associate boundaries with our simulation.
sim.add_boundary_conditions(boundaries)

sim.solver.max_iterations = 8000
sim.solver.absolute_tolerance = 0.0
sim.solver.relative_tolerance = 1e-14
sim.solver.preconditioner = True

sim.validate()

sn.api.run(
    input_file    = sim,
    site_name     = site_name,
    output_folder = '../solutions/gravity/',
    overwrite     = True,
    ranks         = ranks,
    wall_time_in_seconds = 3600,
          )
