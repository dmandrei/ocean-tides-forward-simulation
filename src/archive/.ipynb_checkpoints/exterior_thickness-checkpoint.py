#!/usr/bin/env python
# coding: utf-8

# In[8]:


import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import time
import random
import sys

from classes.poisson import poisson_solver

import matplotlib
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 16}

matplotlib.rc('font', **font)


# In[ ]:


buf_comp = {}
for buf in [5,10,15,20,25]:#[1,5,10,15,20,25,30,35,40]:
    a = poisson_solver(meshfile = '../meshes/gravity/mesh.h5')
    a.config.run.tensor_order = 4
    a.config.gravity_run.boundary_condition = "dirichlet"
    a.construct_mesh(nex = 15, buffer=buf)
    a.add_oneD_solution(npoints = 100)
    a.prepare_mesh_fields()
    a.write_mesh(path = a.meshfile)
    a.run_simulation()
    buf_comp[str(buf)] = a.compare_solution_with_analytical()


# In[10]:


xvect, yvect = list(), list()
for key in buf_comp.keys():
    xvect.append(int(key))
    yvect.append(buf_comp[key])
plt.semilogy(xvect, np.abs(yvect), '-')
plt.xlabel("buffer thickness, elements")
plt.ylabel("$\dfrac{||\int \phi d\Omega - \int \phi_{analyt} d\Omega||_2}{||\int \phi_{analyt} d\Omega||_2}$, %")
plt.savefig("buffer_thickness.pdf")


# In[ ]:




