import numpy as np
import os
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import pandas as pd
import h5py
import sys

from salvus.mesh.models_1D import model 
from salvus.mesh import simple_mesh
from salvus.flow import api
import salvus.namespace as sn
import salvus.mesh
from salvus.flow import api, simple_config

sys.path.insert(1, './mass/')
from elemental_matrices import get_mass_matrix

import config_class 

#ocean tides load modelclass config:
def get_analytical_potential(mesh, MODEL, R_Earth):
    m = model.built_in(MODEL)
    potential_field = m.get_gravitational_potential(np.linalg.norm(mesh.points, axis=1)/R_Earth)[mesh.connectivity]
    mesh.attach_field('grav_potential_analytical', potential_field)
    
class config:
    def __init__(self):
        self.const        = config_class .const()
        self.input_files  = config_class .input_files()
        self.run          = config_class .run()
        self.gravity_run  = config_class .gravity_run()
        self.elastos_run  = config_class .elastos_run()
        
class oneD:
    def __init__(self, N_points, max_radius, MODEL, R_Earth):
        m             = model.built_in(MODEL)
        self.name     = MODEL
        self.radii_relative = np.logspace(5,np.log10(max_radius), N_points)/R_Earth
        self.grav_pot = m.get_gravitational_potential(self.radii_relative)
        self.grav_acc = m.get_gravity(self.radii_relative)

class octiload:
    def __init__(self, meshfile = '../meshes/mesh.h5'):
        self.config   = config()
        self.meshfile = meshfile
        if not os.path.exists(os.path.dirname(meshfile)): os.makedirs(os.path.dirname(meshfile))
        self.elastostatic = elastostatic_setup_and_solve()
        self.poisson      = poisson_setup_and_solve()
        
    def construct_mesh(self, with_surface_topo = False, with_moho_topo = False, nex = 18, buffer = 20,
                       oneD_model = "prem_iso_one_crust",):
        
        assert type(with_surface_topo)   == bool
        assert type(with_moho_topo)      == bool
        assert type(nex)    == int
        assert type(buffer) == int
        
        self.MODEL                    = oneD_model
        self.external_layer_thickness = buffer
        self.with_surface_topo        = with_surface_topo
        self.with_moho_topo           = with_moho_topo
        self.oneD_model               = oneD_model
        
        ms = simple_mesh.TidalLoading()
        ms.basic.tidal_loading_file   = self.config.input_files.ocean_tides_model
        ms.basic.tidal_loading_lmax_1 = 256
        ms.basic.tidal_loading_lmax_2 = 256
        ms.basic.nex = nex
        ms.basic.local_refinement_level  = 0
        ms.basic.global_refinement_level = 0
        ms.basic.refinement_threshold    = 0.05
        ms.basic.model = oneD_model

        ms.advanced.tensor_order = self.config.run.tensor_order
        
        if buffer > 0:
            ms.gravity_mesh.add_exterior_domain  = True
            ms.gravity_mesh.nelem_buffer_surface = 2
            ms.gravity_mesh.nelem_buffer_outer   = buffer
            ms.gravity_mesh.dr_basis             = 1.5

        if with_surface_topo:
            assert(exists(path_to_surface_topo))
            ms.topography.topography_file    = self.config.input_files.surface_topography
            ms.topography.topography_varname = 'topography_earth2014_egm2008_lmax_256_lmax_256'
        if with_moho_topo:
            assert(exists(path_to_moho_topo))
            ms.topography.moho_topography_file    = self.config.input_files.moho_topography
            ms.topography.moho_topography_varname = 'moho_topography_crust_1_0_egm2008_lmax_256'

        self.mesh = ms.create_mesh(verbose=True)
        self.mesh_array_shape = (self.mesh.nelem, self.mesh.nodes_per_element)
        self.fields = self.mesh.elemental_fields.keys()
        
    def prepare_mesh_fields(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        mesh = self.mesh
        
        #adding M0 and M1 elemental field
        M1 = (4 * self.config.const.G * np.pi)**-1
        f =  np.ones(self.mesh_array_shape)
        mesh.attach_field('M0', f)
        mesh.attach_field('M1', f * M1)
        
        if ('RHO' in self.fields): mesh.attach_field('RHS', mesh.elemental_fields['RHO'])
        
        #removing all unnessesary fields
        topreserve = ['layer', 'external', 'RHS', 'M0', 'M1']
        for field in list(self.fields):
            if field not in topreserve:
                del mesh.elemental_fields[field]
        
        if ( {'external', 'RHS'}.issubset(set(self.fields)) ):
            mask = np.array(mesh.elemental_fields['external'], dtype = bool)
            mesh.elemental_fields['RHS'][mask] = 0.

        mesh.attach_field('fluid', np.ones(mesh.nelem))
        mesh.map_nodal_fields_to_element_nodal()
        if self.config.gravity_run.boundary_condition == "neumann":
            assert hasattr(self, "oneD_solution"),  "There is no 1D solution in the object. Run 'add_oneD_solution()' first"
            mesh.attach_field('NEUMANN', -1 * self.oneD_solution.grav_acc[-1] * np.ones_like(mesh.elemental_fields['M0']))
        print("Elemental fields in the mesh: ", set(self.mesh.elemental_fields.keys()))
        
    def add_oneD_solution(self, npoints = 500):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        max_radius = max(np.linalg.norm(self.mesh.points, axis=-1))
        self.max_radius    = max_radius
        self.oneD_solution = oneD(N_points=npoints, max_radius=max_radius, MODEL=self.oneD_model, R_Earth=self.config.const.R_Earth)
    
    def plot_oneD_solution(self, ymax = -100):
        assert hasattr(self, "oneD_solution"),  "There is no 'oneD_solution' in the object. Run 'add_oneD_solution' first"
        fig, ax = plt.subplots(1, 2, sharey=True, figsize=[12,12])
        radii        = self.oneD_solution.radii_relative * self.R_Earth
        potential    = self.oneD_solution.grav_pot
        acceleration = self.oneD_solution.grav_acc
        ax[0].plot(potential, radii)
        ax[0].hlines(self.R_Earth,  xmin = np.min(potential), xmax = np.max(potential), color='k', linestyle='--', linewidth=3.,)
        ax[1].plot(acceleration, radii)
        ax[1].hlines(self.R_Earth, xmin = np.min(acceleration), xmax = np.max(acceleration), color='k', linestyle='--', linewidth=3.,)
        
        if ymax > 0:
            for a in ax: a.set_ylim(0, ymax)
        
        ax[0].set_xlabel('grav potential')
        ax[1].set_xlabel('grav acceleration')
        ax[0].set_ylabel('distance from the center of the Earth')
        fig.suptitle("1D solutions")
    
    def read_gps_stations(self, path):
        '''Reads info about gps stations either from one file or from the whole directory. 
            par: path - path to file or directory. All files in the directory will be read. '''
        if os.path.isfile(path):
            stations = pd.read_csv(path, delim_whitespace=True)
            receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                                 side_set_name='r1', station_code=name, fields=self.config.gravity_run.point_solution_fields) 
                                                 for name, lat, lon, depth in zip(stations['Station'],stations['Lat'], stations['Lon'], stations['Height(m)']) ]
        self.stations = receivers
    
    def refine_center_of_the_mesh(self, tolerance=200000.):
        def dist(x):
            return np.linalg.norm(x, axis=-1)
        self.mesh.find_side_sets_generic("r0", dist, tolerance=tolerance)
        print('r0 has been found') if "r0" in self.mesh.side_sets.keys() else print('Resolution is not enough to find this surface. Increase the tolerance')
        
    def run_simulation(self, stations=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"

        mesh                                             = self.mesh
        sim                                              = sn.simple_config.simulation.Poisson(mesh=mesh)
        sim.domain.polynomial_order                      = mesh.shape_order
        sim.physics.poisson_equation.mass_matrix_scaling = False
        
        sim.physics.poisson_equation.right_hand_side.filename = self.meshfile
        sim.physics.poisson_equation.right_hand_side.format   = "hdf5"
        sim.physics.poisson_equation.right_hand_side.field    = "RHS"

        sim.physics.poisson_equation.solution.filename        = os.path.basename(self.config.gravity_run.volume_solution)
        sim.physics.poisson_equation.solution.fields          = self.config.gravity_run.volume_solution_fields
        
        bc = self.config.gravity_run.boundary_condition
        if bc == 'dirichlet':
            boundaries = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"])
            sim.add_boundary_conditions(boundaries)
            
        sim.solver.max_iterations     = self.config.gravity_run.max_iterations
        sim.solver.absolute_tolerance = self.config.gravity_run.absolute_tolerance
        sim.solver.relative_tolerance = self.config.gravity_run.relative_tolerance
        sim.solver.preconditioner     = self.config.gravity_run.preconditioner
        
        if type(stations) != bool:
            if hasattr(self, "stations"):
                sim.add_receivers(self.stations) if str(stations) == 'all' else sim.add_receivers(stations)
                sim.output.point_data.format                          = "hdf5"
                sim.output.point_data.filename                        = os.path.basename(self.config.gravity_run.point_solution)
                sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations
        
        sim.validate()

        sn.api.run(
            input_file    = sim,
            site_name     = self.config.run.site_name,
            output_folder = self.config.gravity_run.solution_folder,
            overwrite     = True,
            ranks         = self.config.run.ranks,
            wall_time_in_seconds = self.config.run.wall_time_in_seconds
        )
    
    def prepare_mesh_fields_elastostatic(self, rho_water=1030., real_or_imag ='re'):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        assert (real_or_imag in ['re', 'im'])
        mesh = self.mesh
        self.rho_water = rho_water

        mesh.attach_field('fluid', np.zeros(mesh.nelem))
        mesh.attach_field('zeros', 0. * mesh.elemental_fields['tidal_elevation_amp'])
        mesh.map_nodal_fields_to_element_nodal()
        
        NEUMANN = self.rho_water * self.mesh.elemental_fields['tidal_elevation_'+real_or_imag]
        mesh.attach_field('NEUMANN', NEUMANN)
        #removing all unnessesary fields
        del a.mesh.element_nodal_fields['QMU']
        del a.mesh.element_nodal_fields['QKAPPA']

    def run_simulation_elastostatic(self, stations=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"

        mesh                                             = self.mesh
        sim                                              = sn.simple_config.simulation.Elastostatic(mesh=mesh)
        sim.domain.polynomial_order                      = mesh.shape_order
        sim.physics.elastostatic_equation.mass_matrix_scaling = False
        
        sim.physics.elastostatic_equation.gravity = "full"
        sim.physics.elastostatic_equation.right_hand_side.filename = self.meshfile
        sim.physics.elastostatic_equation.right_hand_side.format   = "hdf5"
        sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros", "zeros", "zeros"]

        sim.physics.elastostatic_equation.solution.filename        = os.path.basename(self.config.elastos_run.volume_solution)
        sim.physics.elastostatic_equation.solution.fields          = self.config.elastos_run.volume_solution_fields
        
        boundaries = simple_config.boundary.Neumann(
            side_sets=["r1"]
        )
        sim.add_boundary_conditions(boundaries)

        sim.solver.max_iterations     = self.config.elastos_run.max_iterations
        sim.solver.absolute_tolerance = self.config.elastos_run.absolute_tolerance
        sim.solver.relative_tolerance = self.config.elastos_run.relative_tolerance
        sim.solver.preconditioner     = self.config.elastos_run.preconditioner
        
        if type(stations) != bool:
            if hasattr(self, "stations"):
                sim.add_receivers(self.stations) if str(stations) == 'all' else sim.add_receivers(stations)
                sim.output.point_data.format                          = "hdf5"
                sim.output.point_data.filename                        = os.path.basename(self.config.elastos_run.point_solution)
                sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations
        
        sim.validate()

        sn.api.run(
            input_file    = sim,
            site_name     = self.config.run.site_name,
            output_folder = self.config.elastos_run.solution_folder,
            overwrite     = True,
            ranks         = self.config.run.ranks,
            wall_time_in_seconds = self.config.run.wall_time_in_seconds
        )
    
    def write_mesh(self, path=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        self.mesh.write_h5(path)
    
    def get_mass_matrix(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        self.mass_matrix = get_mass_matrix(self.mesh)
    
    def integrate_nodal_field(self, field, mesh="earthmesh"):
        if mesh == "earthmesh":
            mm = get_mass_matrix(self.earthmesh)
            return np.sum(self.earthmesh.element_nodal_fields[field]*mm) 
    def read_simulation_results(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        solution = sn.UnstructuredMesh.from_h5(self.config.gravity_run.volume_solution)
        self.mesh.attach_field('grav_potential', solution.element_nodal_fields['solution'])
    
    def get_onlyEarth_mesh(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        mask  = self.mesh.elemental_fields['external'] < 0.5
        self.earthmesh = self.mesh.apply_element_mask(mask)
    
    def get_analytical_potential_earth(self):
        assert hasattr(self, "earthmesh"),  "There is no mesh in the object. Run 'get_onlyEarth_mesh()' first"
        get_analytical_potential(self.earthmesh, self.MODEL, self.config.const.R_Earth)
        
    def compare_solution_with_analytical(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        if not hasattr(self, "mass_matrix"):
            self.get_mass_matrix()
        #reading simulation results from the folder specified in th econfig object
        self.read_simulation_results()
        #take out only the earth out of the domain
        self.get_onlyEarth_mesh()
        #compute analytical gravitational potential for the earth part of the domain
        self.get_analytical_potential_earth()
        
        modeled = self.integrate_nodal_field('grav_potential')
        if np.min(modeled) > 0: modeled *= -1
        analyt  = self.integrate_nodal_field('grav_potential_analytical')
        
        print(f'Diff between modelled and analyt potentials: {np.sum((modeled - analyt))/np.sum(analyt)*100}%',)


class poisson_setup_and_solve_and_process(octiload):
    
    def add_oneD_solution(self, npoints = 500):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        max_radius = max(np.linalg.norm(self.mesh.points, axis=-1))
        self.max_radius    = max_radius
        self.oneD_solution = oneD(N_points=npoints, max_radius=max_radius, MODEL=self.oneD_model, R_Earth=self.config.const.R_Earth)
    
    def plot_oneD_solution(self, ymax = -100):
        assert hasattr(self, "oneD_solution"),  "There is no 'oneD_solution' in the object. Run 'add_oneD_solution' first"
        fig, ax = plt.subplots(1, 2, sharey=True, figsize=[12,12])
        radii        = self.oneD_solution.radii_relative * self.R_Earth
        potential    = self.oneD_solution.grav_pot
        acceleration = self.oneD_solution.grav_acc
        ax[0].plot(potential, radii)
        ax[0].hlines(self.R_Earth,  xmin = np.min(potential), xmax = np.max(potential), color='k', linestyle='--', linewidth=3.,)
        ax[1].plot(acceleration, radii)
        ax[1].hlines(self.R_Earth, xmin = np.min(acceleration), xmax = np.max(acceleration), color='k', linestyle='--', linewidth=3.,)
        
        if ymax > 0:
            for a in ax: a.set_ylim(0, ymax)
        
        ax[0].set_xlabel('grav potential')
        ax[1].set_xlabel('grav acceleration')
        ax[0].set_ylabel('distance from the center of the Earth')
        fig.suptitle("1D solutions")
    
    def run_simulation(self, stations=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"

        mesh                                             = self.mesh
        sim                                              = sn.simple_config.simulation.Poisson(mesh=mesh)
        sim.domain.polynomial_order                      = mesh.shape_order
        sim.physics.poisson_equation.mass_matrix_scaling = False
        
        sim.physics.poisson_equation.right_hand_side.filename = self.meshfile
        sim.physics.poisson_equation.right_hand_side.format   = "hdf5"
        sim.physics.poisson_equation.right_hand_side.field    = "RHS"

        sim.physics.poisson_equation.solution.filename        = os.path.basename(self.config.gravity_run.volume_solution)
        sim.physics.poisson_equation.solution.fields          = self.config.gravity_run.volume_solution_fields
        
        bc = self.config.gravity_run.boundary_condition
        if bc == 'dirichlet':
            boundaries = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"])
            sim.add_boundary_conditions(boundaries)
            
        sim.solver.max_iterations     = self.config.gravity_run.max_iterations
        sim.solver.absolute_tolerance = self.config.gravity_run.absolute_tolerance
        sim.solver.relative_tolerance = self.config.gravity_run.relative_tolerance
        sim.solver.preconditioner     = self.config.gravity_run.preconditioner
        
        if type(stations) != bool:
            if hasattr(self, "stations"):
                sim.add_receivers(self.stations) if str(stations) == 'all' else sim.add_receivers(stations)
                sim.output.point_data.format                          = "hdf5"
                sim.output.point_data.filename                        = os.path.basename(self.config.gravity_run.point_solution)
                sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations
        
        sim.validate()

        sn.api.run(
            input_file    = sim,
            site_name     = self.config.run.site_name,
            output_folder = self.config.gravity_run.solution_folder,
            overwrite     = True,
            ranks         = self.config.run.ranks,
            wall_time_in_seconds = self.config.run.wall_time_in_seconds
        )
        
    def prepare_mesh_fields(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        mesh = self.mesh
        
        #adding M0 and M1 elemental field
        M1 = (4 * self.config.const.G * np.pi)**-1
        f =  np.ones(self.mesh_array_shape)
        mesh.attach_field('M0', f)
        mesh.attach_field('M1', f * M1)
        
        if ('RHO' in self.fields): mesh.attach_field('RHS', mesh.elemental_fields['RHO'])
        
        #removing all unnessesary fields
        topreserve = ['layer', 'external', 'RHS', 'M0', 'M1']
        for field in list(self.fields):
            if field not in topreserve:
                del mesh.elemental_fields[field]
        
        if ( {'external', 'RHS'}.issubset(set(self.fields)) ):
            mask = np.array(mesh.elemental_fields['external'], dtype = bool)
            mesh.elemental_fields['RHS'][mask] = 0.

        mesh.attach_field('fluid', np.ones(mesh.nelem))
        mesh.map_nodal_fields_to_element_nodal()
        if self.config.gravity_run.boundary_condition == "neumann":
            assert hasattr(self, "oneD_solution"),  "There is no 1D solution in the object. Run 'add_oneD_solution()' first"
            mesh.attach_field('NEUMANN', -1 * self.oneD_solution.grav_acc[-1] * np.ones_like(mesh.elemental_fields['M0']))
        print("Elemental fields in the mesh: ", set(self.mesh.elemental_fields.keys()))
        
    def read_simulation_results(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        solution = sn.UnstructuredMesh.from_h5(self.config.gravity_run.volume_solution)
        self.mesh.attach_field('grav_potential', solution.element_nodal_fields['solution'])
    
    def get_onlyEarth_mesh(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        mask  = self.mesh.elemental_fields['external'] < 0.5
        self.earthmesh = self.mesh.apply_element_mask(mask)
    
    def get_analytical_potential_earth(self):
        assert hasattr(self, "earthmesh"),  "There is no mesh in the object. Run 'get_onlyEarth_mesh()' first"
        get_analytical_potential(self.earthmesh, self.MODEL, self.config.const.R_Earth)
        
    def compare_solution_with_analytical(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        if not hasattr(self, "mass_matrix"):
            self.get_mass_matrix()
        #reading simulation results from the folder specified in th econfig object
        self.read_simulation_results()
        #take out only the earth out of the domain
        self.get_onlyEarth_mesh()
        #compute analytical gravitational potential for the earth part of the domain
        self.get_analytical_potential_earth()
        
        modeled = self.integrate_nodal_field('grav_potential')
        if np.min(modeled) > 0: modeled *= -1
        analyt  = self.integrate_nodal_field('grav_potential_analytical')
        
        print(f'Diff between modelled and analyt potentials: {np.sum((modeled - analyt))/np.sum(analyt)*100}%',)
        
class elastostatic_setup_and_solve_and_process(octiload):
    
    def prepare_mesh_fields(self, rho_water=1030., real_or_imag ='re'):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        assert (real_or_imag in ['re', 'im'])
        mesh = self.mesh
        self.rho_water = rho_water

        mesh.attach_field('fluid', np.zeros(mesh.nelem))
        mesh.attach_field('zeros', 0. * mesh.elemental_fields['tidal_elevation_amp'])
        mesh.map_nodal_fields_to_element_nodal()
        
        NEUMANN = self.rho_water * self.mesh.elemental_fields['tidal_elevation_'+real_or_imag]
        mesh.attach_field('NEUMANN', NEUMANN)
        #removing all unnessesary fields
        del a.mesh.element_nodal_fields['QMU']
        del a.mesh.element_nodal_fields['QKAPPA']
    
    def run_simulation(self, stations=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"

        mesh                                             = self.mesh
        sim                                              = sn.simple_config.simulation.Elastostatic(mesh=mesh)
        sim.domain.polynomial_order                      = mesh.shape_order
        sim.physics.elastostatic_equation.mass_matrix_scaling = False
        
        sim.physics.elastostatic_equation.gravity = "full"
        sim.physics.elastostatic_equation.right_hand_side.filename = self.meshfile
        sim.physics.elastostatic_equation.right_hand_side.format   = "hdf5"
        sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros", "zeros", "zeros"]

        sim.physics.elastostatic_equation.solution.filename        = os.path.basename(self.config.elastos_run.volume_solution)
        sim.physics.elastostatic_equation.solution.fields          = self.config.elastos_run.volume_solution_fields
        
        boundaries = simple_config.boundary.Neumann(
            side_sets=["r1"]
        )
        sim.add_boundary_conditions(boundaries)

        sim.solver.max_iterations     = self.config.elastos_run.max_iterations
        sim.solver.absolute_tolerance = self.config.elastos_run.absolute_tolerance
        sim.solver.relative_tolerance = self.config.elastos_run.relative_tolerance
        sim.solver.preconditioner     = self.config.elastos_run.preconditioner
        
        if type(stations) != bool:
            if hasattr(self, "stations"):
                sim.add_receivers(self.stations) if str(stations) == 'all' else sim.add_receivers(stations)
                sim.output.point_data.format                          = "hdf5"
                sim.output.point_data.filename                        = os.path.basename(self.config.elastos_run.point_solution)
                sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations
        
        sim.validate()

        sn.api.run(
            input_file    = sim,
            site_name     = self.config.run.site_name,
            output_folder = self.config.elastos_run.solution_folder,
            overwrite     = True,
            ranks         = self.config.run.ranks,
            wall_time_in_seconds = self.config.run.wall_time_in_seconds
        )