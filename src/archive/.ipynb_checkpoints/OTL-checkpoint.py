# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.12.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Spherical caps benchmark
#
# ### Homogeneous sphere
#
# **Model**
#
# - $v_p = 5.92$ km/s
# - $v_s = 3.42$ km/s
# - $\rho = 3.00$ km/m$^{3}$

# %%writefile model.bm
NAME         homogeneous_sphere
UNITS        km
COLUMNS      depth rho vp vs
    0.0   3.0     5.92   3.42
 6371.0   3.0     5.92   3.42

file = "data/10degCaps_noTaper_Homogeneous_Vp05.92_Vs03.42_Rho03.00_analytical_noDeg1_dens1000_nonGravitating.txt"
SALVUS_FLOW_SITE_NAME = "dev"
NUM_RANKS=12
# ELLIPTICITY = True
# TOPOGRAPHY = True
# MOHO_TOPOGRAPHY = True

# ## Python imports

# +
# %config Completer.use_jedi = False
# %matplotlib inline
import matplotlib.pyplot as plt
plt.style.use('ggplot')
plt.rcParams['figure.figsize'] = 12, 8

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from salvus.flow import api, simple_config
from salvus.mesh import simple_mesh
# -

# ## Load data and validate receiver locations

# +
with open(file, "r") as f:
    header = f.readline()
    
headers = header[:-1].split(" | ")

stations = pd.read_csv(file,
   skiprows=1,
   header=None,
   names=headers,
   delimiter=' ',
   skipinitialspace=True,
)
stations
# -

recs = [
    simple_config.receiver.seismology.SideSetPoint3D(
        latitude=lat,
        longitude=lon,
        depth_in_m=0.0,
        radius_of_sphere_in_m=6371000.0,
        side_set_name="r1",
        network_code="",
        station_code=name,
        location_code="",
        fields=["velocity"],
    )
    for name, lat, lon in zip(
        stations['Station'], 
        stations['Lat(+N,deg)'], 
        stations['Lon(+E,deg)']
    )
]


# +
def cmp2str(c):
    if c == "V":
        return "vertical"
    if c == "N":
        return "North"
    if c == "E":
        return "East"
    
    raise ValueError("`c` must be one of ['V', 'N', 'E']")

fig, ax = plt.subplots(3,1, sharex=True, figsize=[20,10])


deg = stations[f"Lat(+N,deg)"]
idx = np.argsort(deg)
deg = deg[idx]

ref = {}
for _i, cmp in enumerate(["V", "N", "E"]):
    ref[cmp] = stations[f"{cmp}-Amp(mm)"] * (90 - stations[f"{cmp}-Pha(deg)"])/ 90 
    ref[cmp] = ref[cmp][idx]
    
for _i, cmp in enumerate(["V", "N", "E"]):    
    ax[_i].plot(deg,ref[cmp])
    ax[_i].set_ylabel("amplitude [mm]")
    ax[_i].set_title(cmp2str(cmp))

plt.show()
# -

# ## Generate mesh

mc = simple_mesh.basic_mesh.Globe3D()
mc.basic.model = "model.bm"
mc.basic.min_period_in_seconds = 150.0
mc.basic.elements_per_wavelength = 2.0
mc.advanced.tensor_order = 1
mesh = mc.create_mesh()
mesh.nelem

# ## Create simulation object

# g = -GM/r**2 = -4/3 pi * rho * r * G
G = 6.67408e-11
g = - 4.0/3.0 * np.pi * 3000 * 6371e3 * G
g

# +
f = np.zeros_like(mesh.elemental_fields["VP"])
# loading_mesh.elemental_fields["fluid"] = np.zeros([mesh.nelem])

# spherical caps
earth_radius_in_meters = 6371000.0
radius_in_degrees = 10.0
height_in_meters = 1.0
density=1000.0

cap_radius_in_meters = np.sin(radius_in_degrees * np.pi / 180.0) * earth_radius_in_meters
p = mesh.get_element_nodes()
rr = np.linalg.norm(p, axis=2)

# select points within the cap radius from the poles
mask = np.sqrt( p[:,:,0] ** 2 + p[:,:,1] ** 2)  < cap_radius_in_meters
f[mask] = 1.0

# only apply caps at the poles
mask = np.abs(rr - earth_radius_in_meters) > 1.0
f[mask] = 0.0
mesh.attach_field("NEUMANN", g * density * height_in_meters * f)
mesh.attach_field("zeros", 0.0 * f)
mesh.write_h5("mesh.h5")


# +
sim = simple_config.simulation.Elastostatic(mesh="mesh.h5")

sim.domain.polynomial_order = mesh.shape_order

sim.physics.elastostatic_equation.gravity = "off"
sim.physics.elastostatic_equation.right_hand_side.filename = "mesh.h5"
sim.physics.elastostatic_equation.right_hand_side.format = "hdf5"
sim.physics.elastostatic_equation.right_hand_side.fields = ["zeros", "zeros", "zeros"]

sim.physics.elastostatic_equation.solution.filename = "solution.h5"
sim.physics.elastostatic_equation.solution.fields = ["solution", "residuals"]

boundaries = simple_config.boundary.Neumann(
    side_sets=["r1"]
)
sim.add_boundary_conditions(boundaries)

# boundaries = simple_config.boundary.HomogeneousDirichlet(
#     side_sets=["r0"]
# )
# w.add_boundary_conditions(boundaries)

sim.solver.max_iterations = 8000
sim.solver.absolute_tolerance = 0.0
sim.solver.relative_tolerance = 1e-15
sim.solver.preconditioner = True

sim.add_receivers(recs)
sim.output.point_data.format = "asdf"
sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations


sim.validate()
# sim
# -

# ! du -ksh mesh.h5

# +
api.run(
    input_file=sim, 
    site_name="tides",
    ranks=12,
    output_folder="re_1", 
    overwrite=True,

)
# -

# ## Postprocess solutions

# !cat re/stdout

import pyasdf
def load_data(hdf5_file):
    
    
    with pyasdf.ASDFDataSet(hdf5_file) as ds:
        
        numRec = len(ds.waveforms)  
        lat = np.zeros(numRec)
        lon = np.zeros(numRec)
        east = np.zeros(numRec)
        north = np.zeros(numRec)
        vertical = np.zeros(numRec)
        
        i=0
        for station in ds.waveforms:
            name = station._station_name.replace('.','')
            lat[i] = stations[stations["Station"]==name][f"Lat(+N,deg)"]
            lon[i] = stations[stations["Station"]==name][f"Lon(+E,deg)"]         
            north[i] = station.velocity.select(component='N')[0].data[-1]
            east[i] = station.velocity.select(component='E')[0].data[-1]
            vertical[i] = station.velocity.select(component='Z')[0].data[-1]        
            i=i+1
    
    return {'lat': lat, 
            'lon': lon, 
            'east': east, 
            'north': north, 
            'vertical': vertical }


result = load_data("re/receivers.h5")

# +
fig, ax = plt.subplots(3,2, sharex=True, figsize=[20,10])
idx = np.argsort(result["lat"])

results = {
    "V": 1000*result["vertical"][idx],
    "N": 1000*result["north"][idx],
    "E": 1000*result["east"][idx],    
 }

for _i, cmp in enumerate(["vertical", "north", "east"]):
    ax[_i,0].plot(result["lat"][idx], 1000*result[cmp][idx])
    ax[_i,0].set_ylabel("amplitude [mm]")
#     ax[_i].set_title(cmp2str(cmp))
for _i, cmp in enumerate(["V", "N", "E"]):
    ax[_i,0].plot(deg, ref[cmp])
    ax[_i,0].set_ylabel("amplitude [mm]")
    ax[_i,0].set_title(cmp2str(cmp))
    ax[_i,1].plot(result["lat"][idx], results[cmp] - ref[cmp])    
plt.show()
# -


fig, ax = plt.subplots(3,1, sharex=True, figsize=[20,10])
idx = np.argsort(result["lat"])
for _i, cmp in enumerate(["vertical", "north", "east"]):
    ax[_i].plot(result["lat"][idx], 1000*result[cmp][idx])
    ax[_i].set_ylabel("amplitude [mm]")
for _i, cmp in enumerate(["V", "N", "E"]):
    ax[_i].plot(stations[f"Lat(+N,deg)"], stations[f"{cmp}-Amp(mm)"] * (90 - stations[f"{cmp}-Pha(deg)"])/ 90 )
    ax[_i].set_ylabel("amplitude [mm]")
    ax[_i].set_title(cmp2str(cmp))
plt.show()


