#!/usr/bin/env python
# coding: utf-8



import salvus.namespace as sn
from salvus.mesh import simple_mesh
import numpy as np

m = simple_mesh.TidalLoading()
m.basic.model = 'prem_iso_one_crust'
m.gravity_mesh.add_exterior_domain = True
m.gravity_mesh.nelem_buffer_outer  = 1
m.advanced.tensor_order = 1
mesh = m.create_mesh()

mesh.attach_field('rhs', mesh.elemental_fields['RHO'])
f =  np.ones_like(mesh.elemental_fields['RHO'])

###############################
mesh.attach_field('fluid', f) #np.ones(mesh.nelem))
#### if 'fluid' field, for example, is not an array of dimensions (nelem, ) but rather has dimensions (nelem, nodes_per_elem), the simulation fails and the error message doesn't seem to be very informative to me:
#                 "All ranks have reported the same error. Message:
#                  Incorrect number of dimensions in HDF5 model file. Expected 1, but got 2 for dataset /MODEL/medium"
###############################

mesh.attach_field('M0', f)
mesh.attach_field('M1', f)
    
for field in ['RHO', 'VP', 'VS', 'QMU', 'QKAPPA', 'GRAD_PHI_X', 'GRAD_PHI_Y', 'GRAD_PHI_Z', 'g']:
    del mesh.elemental_fields[field]

mesh.write_h5('./mesh.h5')

sim                                              = sn.simple_config.simulation.Poisson(mesh=mesh)
sim.domain.polynomial_order                      = mesh.shape_order

sim.physics.poisson_equation.right_hand_side.filename = "./mesh.h5"
sim.physics.poisson_equation.right_hand_side.format   = "hdf5"
sim.physics.poisson_equation.right_hand_side.field    = "rhs"

sim.physics.poisson_equation.solution.filename = "solution.h5"
sim.physics.poisson_equation.solution.fields   = ["solution"]

boundaries = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"])
sim.add_boundary_conditions(boundaries)
sim.solver.max_iterations = 8000
sim.solver.absolute_tolerance = 0.0
sim.solver.relative_tolerance = 1e-14
sim.solver.preconditioner = True

sim.validate()

sn.api.run(
    input_file    = sim,
    site_name     = 'local',
    output_folder = './solution/',
    overwrite     = True,
    ranks         = 10,
          )





