import salvus
import xarray as xr
import pygmt
from salvus.mesh.unstructured_mesh_utils import extract_model_to_regular_grid
import numpy as np

class gmtplot:
    def __init__(self, mesh, field, resolution=101):
        if (type(mesh) == str): 
            self.get_mesh_from_file(mesh)
        else:
            self.mesh  = mesh
        self.field = field
        self.res   = resolution
    
    def get_mesh_from_file(self, meshfile):
        self.mesh = salvus.mesh.UnstructuredMesh.from_h5(meshfile)

    def get_surface_field(self):
        data = xr.Dataset(
            coords={
                "longitude":  np.linspace(-180., 180., self.res),
                "latitude":   np.linspace( -90.,  90., self.res),
                "depth":      np.linspace(1., 
                                          1., 1),
            },
            attrs={"radius_in_meters": 6371e3},
        )
        self.dataset =  extract_model_to_regular_grid(self.mesh, 
                                                      data, [self.field], 
                                                      verbose=True)

    def get_dataarray(self,):
        self.da = xr.DataArray(data = self.dataset[self.field][:,:,0],
                                 dims = ['lat', 'lon'],
                                 coords = dict(
                                    lat = (np.array(self.dataset['latitude'])),
                                    lon = (np.array(self.dataset['longitude']))
                              ))

    def plot(self, name, limits=[], save=False):
        da = self.da
        fig = pygmt.Figure()
        fig.basemap(region='d', projection="R20c", frame=["a",f'+t"{name}"'])
        if len(limits) != 2: limits = [float(np.min(da)), float(np.max(da))]
        if limits[0] >= limits[1]: limits[0] -= 1e-5 * np.min(np.abs(limits))
        pygmt.makecpt(cmap="dem2", series=limits)
        fig.grdimage(grid=da)
        fig.coast(shorelines="0.5p,black")
        fig.colorbar(frame=["a","y+lX",],)
        fig.show()
        if save: fig.savefig(f'{name}.pdf')
        return fig
