#!/usr/bin/env python
# coding: utf-8

# In[6]:


import salvus.namespace as sn

shape_order = 2
dimension   = 3
volume_solution='../solutions/gravity/solution.h5'
volume_solution_fields = ['solution', 'residuals', 'right-hand-side']
remote_path = "REMOTE:/scratch/snx3000/admitrov/mesh.h5"

sim                                              = sn.simple_config.simulation.Poisson()
sim.domain.polynomial_order                      = shape_order
sim.domain.dimension                             = 3

sim.domain.geometry.format                       = "hdf5"
sim.domain.geometry.filename                     = remote_path
sim.domain.mesh.format                       = "hdf5"
sim.domain.mesh.filename                     = remote_path
sim.domain.model.format                      = "hdf5"
sim.domain.model.filename                    = remote_path



sim.physics.poisson_equation.mass_matrix_scaling = False
sim.physics.poisson_equation.right_hand_side.filename = remote_path
        
sim.physics.poisson_equation.right_hand_side.format   = "hdf5"
sim.physics.poisson_equation.right_hand_side.field    = "RHS"
 
sim.physics.poisson_equation.solution.filename        = 'solution.h5'
sim.physics.poisson_equation.solution.fields          = volume_solution_fields


boundaries = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"], value=float(0))
print("external homogeneous Dirichlet boundary condition: ", float(0))
sim.add_boundary_conditions(boundaries)
            
sim.solver.max_iterations     =  3000
sim.solver.absolute_tolerance =  0.0 
sim.solver.relative_tolerance =  1e-9
sim.solver.preconditioner     =  True
        
sim.validate()

sn.api.run(
    input_file    = sim,
    site_name     = 'daint',
    output_folder = '../solutions/gravity/',
    overwrite     = True,
    ranks         = 120,
    wall_time_in_seconds = 3600,
          )

