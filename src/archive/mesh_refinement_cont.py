#!/usr/bin/env python
# coding: utf-8



import time
import os
import numpy as np

from salvus.mesh.models_1D import model 
from salvus.mesh import UnstructuredMesh
from salvus.mesh.tools.transforms import uniformly_refine_chunkwise
from classes.poisson import poisson_solver
import classes.tools as ts
import subprocess



to = 1
a = poisson_solver(meshfile = '/home/andreid/projects/ocean-tides-forward-simulation/meshes/gravity/mesh.h5')
a.config.run.tensor_order = 1 # has to be 1 to further refine. Will be changed later

a.mesh = UnstructuredMesh.from_h5(a.meshfile) 
a.oneD_model = "prem_iso_one_crust"
for param in ['RHO']:
    ts.reassign_parameter(a.mesh, parameter='RHO', oneD_model=a.oneD_model)

mass_matrix = ts.get_mass_matrix(a.mesh)
mass = np.sum(mass_matrix * a.mesh.elemental_fields['RHO'])

a.prepare_mesh_fields()
a.write_mesh(a.meshfile)

host = "daint.cscs.ch"
remote_path = "/scratch/snx3000/admitrov/mesh.h5"
a.config.run.ranks=1200
a.config.run.site_name='daint'
#uploading mesh to piz daint
subprocess.run(['./scp_mesh.sh', a.meshfile, host, remote_path])
a.run_simulation(remote_path=remote_path)

a.read_simulation_results()
mass_matrix = ts.get_mass_matrix(a.mesh)
solution_integral[a.mesh.nelem] = {'mass': mass,
                                   'tensor order': to,
                                   'grav potential integral': ts.get_parameter_integral(mesh=a.mesh, param='grav_potential', mass_matrix=mass_matrix, mask = (a.mesh.elemental_fields['external']==0))}
print(solution_integral)
with open('solutions_integral_cont.txt', 'w') as f:
    f.write(str(solution_integral))
