import time
import os
import numpy as np

from salvus.mesh.models_1D import model 
from salvus.mesh import UnstructuredMesh
from salvus.mesh.tools.transforms import uniformly_refine_chunkwise
from classes.poisson import poisson_solver
import classes.tools as ts
import subprocess

ro = 2 #refinement order
n_refinements = 1
solution_integral = {}

host = "daint.cscs.ch"
remote_path = "/scratch/snx3000/admitrov/mesh.h5"

for to in [1]:
    a = poisson_solver(meshfile = '/home/andreid/projects/ocean-tides-forward-simulation/meshes/gravity/mesh.h5')
    a.config.run.tensor_order = to # has to be 1 to further refine. Will be changed later
    a.mesh = UnstructuredMesh.from_h5(a.meshfile)
    for field in list(a.fields - set(['layer', 'external'])):
        del a.mesh.elemental_fields[field]
    
    for dummy in range(n_refinements+1):
        if dummy > 0:
            test =  uniformly_refine_chunkwise(
                mesh=a.mesh,
                output_filename=a.meshfile,
                nchunk=8 ** (dummy+1),
                uniformly_refine_kwargs={
                    "tensor_order":to,
                    "refinement_order":ro
                }
            )
            a.mesh = UnstructuredMesh.from_h5(a.meshfile) 
            
            for param in ['RHO']:
                ts.reassign_parameter(a.mesh, parameter='RHO', oneD_model=a.oneD_model)
            
            mass_matrix = ts.get_mass_matrix(a.mesh)
            mass = np.sum(mass_matrix*a.mesh.elemental_fields['RHO']) 
            print('mass/M_earth', mass/5.972/10**24)
            a.prepare_mesh_fields()
            a.add_oneD_solution(npoints = 100)
            a.write_mesh(a.meshfile)
            
            a.config.run.ranks=120
            a.config.run.site_name='daint'
            
            subprocess.run(['./scp_mesh.sh', a.meshfile, host, remote_path])
            a.run_simulation(remote_path=remote_path)
            
            a.read_simulation_results()
            solution_integral[str(a.mesh.nelem)] = {'grav_pot integral': ts.get_parameter_integral(mesh=a.mesh, param='grav_potential', mass_matrix=mass_matrix, mask = (a.mesh.elemental_fields['external']==0)), 'mass': mass}
            
            with open('solution_integral_continue.txt', 'w') as f:
                f.write(str(solution_integral))

